# test-bago-frontend

Consigna:
- Armar una pantalla que liste las farmacias activas de la cadena FARMACITY dado un formulario de una farmacia de alta, agregar el teléfono al mismo

Instructivo antes de arrancar:
- Bajar el proyecto de https://gitlab.com/mobasilico/test-bago-frontend
- Abrir VSCode (ejemplo) y levantar el proyecto de la carpeta test-bago-frontend con un docker-compose up
- Una vez levantado el docker, desarrollar en localhost:4001
- Abrir el moockon-portable.exe. Presionar open environment, seleccionar el archivo farmacias.json y por ultimo darle Start
