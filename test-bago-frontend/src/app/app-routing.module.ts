import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AltaComponent } from './alta/alta.component';
import { OneComponent } from './one/one.component';
import { TwoComponent } from './two/two.component';
import { VerComponent } from './ver/ver.component';


const routes: Routes = [
  { path: 'one', component: OneComponent },
  { path: 'two', component: TwoComponent },
  { path: 'ver', component: VerComponent },
  { path: 'alta', component: AltaComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
