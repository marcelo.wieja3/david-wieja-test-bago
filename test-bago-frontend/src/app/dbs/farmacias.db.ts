import { Farmacia } from "../models/farmacia.model";

export const farmacias: Farmacia[] = [
    {
        "id": 140237,
        "nombre": "TRAFUL",
        "estado": true,
        "id_cadena": 0
    },
    {
        "id": 160368,
        "nombre": "SALUDFARMA SRL",
        "estado": true,
        "id_cadena": 0
    },
    {
        "id": 160371,
        "nombre": "NUEVA FARMA",
        "estado": true,
        "id_cadena": 0
    },
    {
        "id": 170286,
        "nombre": "INDOSUR",
        "estado": true,
        "id_cadena": 0
    },
    {
        "id": 180218,
        "nombre": "LUJAN",
        "estado": false,
        "id_cadena": 0
    },
    {
        "id": 180253,
        "nombre": "AGUILA DOS",
        "estado": false,
        "id_cadena": 0
    },
    {
        "id": 220345,
        "nombre": "JUSTINO PEREZ 2",
        "estado": true,
        "id_cadena": 0
    },
    {
        "id": 240283,
        "nombre": "VIRGEN DEL VALLE",
        "estado": true,
        "id_cadena": 0
    },
    {
        "id": 250191,
        "nombre": "DE LA ROSA",
        "estado": true,
        "id_cadena": 0
    },
    {
        "id": 280327,
        "nombre": "FUCHS",
        "estado": true,
        "id_cadena": 0
    },
    {
        "id": 74730,
        "nombre": "DEL AGUILA PALMARES",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 74736,
        "nombre": "DEL AGUILA DEL HOSPITAL",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 74839,
        "nombre": "MITRE 2",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 74842,
        "nombre": "MITRE 1",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 75060,
        "nombre": "MITRE 25",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 75718,
        "nombre": "MITRE 4",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 78067,
        "nombre": "MITRE 7",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 300123,
        "nombre": "FARMACITY (LOPE DE VEGA)",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 210187,
        "nombre": "FARMACITY 128 FSA.",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 460420,
        "nombre": "FARMACITY ILLIA",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 300183,
        "nombre": "FARMACITY CHICLANA",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 300195,
        "nombre": "FARMACITY ASAMBLEA",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 300206,
        "nombre": "FARMACITY LARRAZABAL",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 459019,
        "nombre": "FARMACITY-JUJUY",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 461223,
        "nombre": "FARMACITY GAONA",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 461220,
        "nombre": "FARMACITY SAN PEDRITO",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 461616,
        "nombre": "FARMACITY (168)",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 467616,
        "nombre": "FARMACITY (175 ) V. MARIA",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 300178,
        "nombre": "FARMACITY BOEDO",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 300181,
        "nombre": "FARMACITY JUJUY",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 300182,
        "nombre": "FARMACITY CASEROS",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 300204,
        "nombre": "FARMACITY SAN JUAN",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 461225,
        "nombre": "FARMACITY NAZCA 1978",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 220176,
        "nombre": "FARMACITY (109)",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 460418,
        "nombre": "FARMACITY VILLA MERCEDES II",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 76082,
        "nombre": "DEL AGUILA(S.M.)",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 76072,
        "nombre": "AVENIDA(CDAD.)",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 76762,
        "nombre": "FARMACITY III",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 77581,
        "nombre": "MERCANTIL",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 76099,
        "nombre": "MITRE 8(A.A.C.)",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 75814,
        "nombre": "DEL AGUILA(L.H.)",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 74481,
        "nombre": "FARMACITY SANT.",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 300120,
        "nombre": "FARMACITY BEIRO 2",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 230149,
        "nombre": "FARMACITY SALTA",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 120099,
        "nombre": "FARMACITY SAN LUIS",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 120100,
        "nombre": "FARMACITY VILLA MERCEDES",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 220181,
        "nombre": "FARMACITY (90)",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 210177,
        "nombre": "FARMACITY 104 R",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 210178,
        "nombre": "FARMACITY 125 R",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 300107,
        "nombre": "FARMACITY BEIRO",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 300122,
        "nombre": "FARMACITY ALBERDI",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 210182,
        "nombre": "FARMACITY 126 R",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 140071,
        "nombre": "FARMACITY  58",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 250089,
        "nombre": "MITRE 27",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 300106,
        "nombre": "FARMACITY LACARRA",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 140133,
        "nombre": "FARMACITY 101",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 230144,
        "nombre": "FARMACITY",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 140085,
        "nombre": "FARMACITY  75",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 300121,
        "nombre": "FARMACITY CUENCA II",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 220127,
        "nombre": "FARMACITY (86)",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 457416,
        "nombre": "FARMACITY PEATONAL S.LUIS",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 78184,
        "nombre": "FARMACITY LINIERS",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 300006,
        "nombre": "FARMACITY",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 250033,
        "nombre": "MITRE 16(CTO)",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 77870,
        "nombre": "MITRE 11(DGO)",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 220028,
        "nombre": "FARMACITY (34)",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 77972,
        "nombre": "MITRE 9(5TA.)",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 466420,
        "nombre": "FARMACITY-JUJUY 1",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 140193,
        "nombre": "FARMACITY 266",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 300003,
        "nombre": "FARMACITY CUENCA",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 220294,
        "nombre": "FARMACITY (259)",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 300227,
        "nombre": "FARMACITY ONCE",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 300245,
        "nombre": "FARMACITY ONCE II",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 463243,
        "nombre": "FARMACITY 155 R",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 463244,
        "nombre": "FARMACITY 163 R",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 463245,
        "nombre": "FARMACITY 174 R",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 463242,
        "nombre": "FARMACITY 148 R",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 240219,
        "nombre": "FARMACITY-JUJUY 2",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 220296,
        "nombre": "FARMACITY  SAN FCO.",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 300244,
        "nombre": "FARMACITY DIRECTORIO",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 300237,
        "nombre": "FARMACITY LA RIOJA",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 240238,
        "nombre": "FARMACITY SANT. II",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 300209,
        "nombre": "FARMACITY POMPEYA",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 220325,
        "nombre": "FARMACITY (306)",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 300251,
        "nombre": "FARMACITY EVA PERON",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 140204,
        "nombre": "FARMACITY 300",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 140218,
        "nombre": "FARMACITY 295",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 210184,
        "nombre": "FARMACITY 105 P",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 210185,
        "nombre": "FARMACITY 102 P",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 210186,
        "nombre": "FARMACITY 113 P",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 140140,
        "nombre": "FARMACITY 131",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 140089,
        "nombre": "FARMACITY  79",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 140093,
        "nombre": "FARMACITY  83",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 450417,
        "nombre": "FARMACITY ARTIGAS",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700074,
        "nombre": "FARMACITY LAVALLE",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700075,
        "nombre": "FARMACITY MALABIA",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700076,
        "nombre": "FARMACITY CALLAO",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700077,
        "nombre": "FARMACITY SANTA FÉ",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700078,
        "nombre": "FARMACITY PRIMERA JUNTA",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700079,
        "nombre": "FARMACITY JUNIN",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700073,
        "nombre": "FARMACITY PUEYRREDON",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700080,
        "nombre": "FARMACITY CARABOBO",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700081,
        "nombre": "FARMACITY PANAMA",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700082,
        "nombre": "FARMACITY TRIUNVIRATO",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700083,
        "nombre": "FARMACITY CABILDO",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700084,
        "nombre": "FARMACITY LAS HERAS",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700085,
        "nombre": "FARMACITY ALSINA",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700086,
        "nombre": "FARMACITY FLORIDA",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700087,
        "nombre": "FARMACITY VIDT",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700088,
        "nombre": "FARMACITY MEDRANO",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700089,
        "nombre": "FARMACITY LARREA",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700090,
        "nombre": "FARMACITY DIAGONAL",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700091,
        "nombre": "FARMACITY CASTRO BARROS",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700092,
        "nombre": "FARMACITY LA PLATA",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700093,
        "nombre": "FARMACITY RODRIGUEZ PEÑA",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700095,
        "nombre": "FARMACITY LACROZE",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700096,
        "nombre": "FARMACITY TRIBUNALES",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700097,
        "nombre": "FARMACITY FLORIDA II",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700098,
        "nombre": "FARMACITY L. M. CAMPOS",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700099,
        "nombre": "FARMACITY BUSTAMANTE",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700100,
        "nombre": "FARMACITY OLLEROS",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700101,
        "nombre": "FARMACITY VICENTE LOPEZ",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700102,
        "nombre": "FARMACITY RECONQUISTA",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700103,
        "nombre": "FARMACITY RECOLETA",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700104,
        "nombre": "FARMACITY URUGUAY",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700105,
        "nombre": "FARMACITY URIBURU",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700106,
        "nombre": "FARMACITY PALERMO",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700107,
        "nombre": "FARMACITY ESMERALDA",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700108,
        "nombre": "FARMACITY TALCAHUANO",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700109,
        "nombre": "FARMACITY GUISE",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700110,
        "nombre": "FARMACITY JURAMENTO",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700111,
        "nombre": "FARMACITY INCAS",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700112,
        "nombre": "FARMACITY AV DE MAYO",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700113,
        "nombre": "FARMACITY PUEYRREDON III",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700115,
        "nombre": "FARMACITY CONGRESO",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700116,
        "nombre": "FARMACITY LIBERTAD",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700117,
        "nombre": "FARMACITY ELCANO",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700118,
        "nombre": "FARMACITY JUNCAL",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700119,
        "nombre": "FARMACITY CRAMER",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700120,
        "nombre": "FARMACITY LAS HERAS III",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700121,
        "nombre": "FARMACITY ALEM",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700122,
        "nombre": "FARMACITY PAROISSIEN",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700123,
        "nombre": "FARMACITY MADERO II",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700124,
        "nombre": "FARMACITY CANNING",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700125,
        "nombre": "FARMACITY ARIAS",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700126,
        "nombre": "FARMACITY LA BIELA",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700127,
        "nombre": "FARMACITY MAIPU",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700128,
        "nombre": "FARMACITY MAURE",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700129,
        "nombre": "FARMACITY QUESADA",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700130,
        "nombre": "FARMACITY SOLER",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700131,
        "nombre": "FARMACITY SARMIENTO",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700132,
        "nombre": "FARMACITY PEDRAZA",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700133,
        "nombre": "FARMACITY RIOBAMBA",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700134,
        "nombre": "FARMACITY ABASTO",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700135,
        "nombre": "FARMACITY LIBERTADOR",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700136,
        "nombre": "FARMACITY GOYENA",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700137,
        "nombre": "FARMACITY CHORROARIN",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700138,
        "nombre": "FARMACITY HONDURAS",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700139,
        "nombre": "FARMACITY LA PLATA II",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700140,
        "nombre": "FARMACITY ALBERDI",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700141,
        "nombre": "FARMACITY FLORIDA III",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700142,
        "nombre": "FARMACITY V DEL PINO",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700144,
        "nombre": "FARMACITY GUEMES",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700145,
        "nombre": "FARMACITY CID",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700146,
        "nombre": "FARMACITY TRINVIRATO III",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700143,
        "nombre": "FARMACITY MELINCUE",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700147,
        "nombre": "FARMACITY SUAREZ",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700148,
        "nombre": "FARMACITY BILLINGHURTS",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700149,
        "nombre": "FARMACITY PUJOL",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700150,
        "nombre": "FARMACITY GAONA",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700151,
        "nombre": "FARMACITY MARTINEZ",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700152,
        "nombre": "FARMACITY BELGRANO",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700153,
        "nombre": "FARMACITY ZABALA",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700154,
        "nombre": "FARMACITY DEFENSA",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700155,
        "nombre": "FARMACITY J B JUSTO",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700156,
        "nombre": "FARMACITY NEWBERY",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700157,
        "nombre": "FARMACITY PEÑA",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700158,
        "nombre": "FARMACITY BCO DE BRASIL",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700159,
        "nombre": "FARMACITY GASCÓN",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700160,
        "nombre": "FARMACITY MADERO III",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700161,
        "nombre": "FARMACITY URQUIZA",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700162,
        "nombre": "FARMACITY ROSETTI",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700163,
        "nombre": "FARMACITY MENDOZA",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700164,
        "nombre": "FARMACITY AVELLANEDA",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700165,
        "nombre": "FARMACITY SERRANO",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700166,
        "nombre": "FARMACITY PERÚ",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700167,
        "nombre": "FARMACITY SALVADORES",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700168,
        "nombre": "FARMACITY MOLDES",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700170,
        "nombre": "FARMACITY VERNET",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700171,
        "nombre": "FARMACITY BALBIN",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700172,
        "nombre": "SIMPLICITY SAN ISIDRO",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700173,
        "nombre": "FARMACITY PELLEGRINI",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700174,
        "nombre": "SIMPLICITY MORON",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700175,
        "nombre": "FARMACITY AV DE MAYO II",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700176,
        "nombre": "FARMACITY ARCOS",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700177,
        "nombre": "GET THE LOOK PIEDRAS",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700178,
        "nombre": "FARMACITY YRIGOYEN",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700179,
        "nombre": "SIMPLICITY MONTE GRANDE",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700180,
        "nombre": "GET THE LOOK MONTEVIDEO",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700181,
        "nombre": "FARMACITY CONSTITUYENTES",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700182,
        "nombre": "FARMACITY SAN MARTIN",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700183,
        "nombre": "FARMACITY LA BOCA",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700184,
        "nombre": "FARMACITY ENTRE RÍOS",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700185,
        "nombre": "GET THE LOOK ECHEVERRÍA",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700187,
        "nombre": "FARMACITY MONROE",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700188,
        "nombre": "FARMACITY PATRICIOS",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700189,
        "nombre": "SIMPLICITY MERLO",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700190,
        "nombre": "SIMPLICITY SAN FERNANDO",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700191,
        "nombre": "GET THE LOOK SALVADOR",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700192,
        "nombre": "GET THE LOOK MARCELO T.",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700193,
        "nombre": "FARMACITY OROÑO",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700194,
        "nombre": "SIMPLICITY SAN MIGUEL",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700195,
        "nombre": "SIMPLICITY L DE ZAMORA",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700196,
        "nombre": "SIMPLICITY MORENO",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700197,
        "nombre": "GET THE LOOK LA PLATA",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700198,
        "nombre": "GET THE LOOK URUGUAY",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700199,
        "nombre": "FARMACITY RIOBAMBA II",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700201,
        "nombre": "FARMACITY ACOYTE",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700202,
        "nombre": "SIMPLICITY QUILMES",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700203,
        "nombre": "FARMACITY MELIAN",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700204,
        "nombre": "SIMPLICITY OLIVOS",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700205,
        "nombre": "SIMPLICITY SCALABRINI ORTIZ",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700206,
        "nombre": "SIMPLICITY FLORENCIO VARELA",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700207,
        "nombre": "SIMPLICITY VIRREYES",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700208,
        "nombre": "SIMPLICITY SAN JUSTO",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700209,
        "nombre": "GET THE LOOK AUSTRIA",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700210,
        "nombre": "SIMPLICITY CASEROS",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700211,
        "nombre": "SIMPLICITY RAMOS MEJÍA",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700212,
        "nombre": "SIMPLICITY BANFIELD",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700213,
        "nombre": "SIMPLICITY ADROGUE",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700214,
        "nombre": "SIMPLICITY AVELLANEDA",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700215,
        "nombre": "SIMPLICITY CAMPANA",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700216,
        "nombre": "SIMPLICITY SOLANO",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700217,
        "nombre": "FARMACITY DONATO",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700218,
        "nombre": "SIMPLICITY MUNRO",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700219,
        "nombre": "GET THE LOOK MAIPU",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700220,
        "nombre": "SIMPLICITY SAN ISIDRO II",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700222,
        "nombre": "GET THE LOOK AVELLANEDA",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700223,
        "nombre": "SIMPLICITY MORON III",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700224,
        "nombre": "SIMPLICITY HURLINGAM",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700225,
        "nombre": "FARMACITY BAEZ",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700226,
        "nombre": "GET THE LOOK GALERIAS PACÍFICO",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700227,
        "nombre": "SIMPLICITY BALLESTER",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700228,
        "nombre": "FARMACITY CONSTITUCIÓN",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700229,
        "nombre": "FARMACITY PASTEUR",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700230,
        "nombre": "FARMACITY ALVAREZ JONTE",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700231,
        "nombre": "FARMACITY LA PAMPA II",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700232,
        "nombre": "FARMACITY J. M MORENO",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700233,
        "nombre": "FARMACITY MEMBRILLAR",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700234,
        "nombre": "FARMACITY CHILAVERT",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700235,
        "nombre": "SIMPLICITY PERGAMINO",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700236,
        "nombre": "FARMACITY ESTADO DE ISRAEL",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700237,
        "nombre": "FARMACITY HUMBOLDT",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700238,
        "nombre": "SIMPLICITY JOSE C. PAZ",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700239,
        "nombre": "FARMACITY ER GUALEGUAY",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700240,
        "nombre": "FARMACITY ER PARANA",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700241,
        "nombre": "GET THE LOOK SOLAR",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700242,
        "nombre": "GET THE LOOK ESPEJO",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700243,
        "nombre": "FARMACITY CASEROS",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700244,
        "nombre": "SIMPLICITY LA PLATA",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700245,
        "nombre": "SIMPLICITY LANÚS",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700246,
        "nombre": "GET THE LOOK MARTINEZ",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700247,
        "nombre": "GET THE LOOK RAMOS MEJÍA",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700248,
        "nombre": "GET THE LOOK AVELLANEDA",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700249,
        "nombre": "GET THE LOOK ARAOZ",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700250,
        "nombre": "GET THE LOOK DOT BAIRES",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700251,
        "nombre": "FARMACITY  MDQ ALBERDI",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700252,
        "nombre": "SIMPLICITY MAR DEL PLATA II",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700253,
        "nombre": "SIMPLICITY PIÑEIRO",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700254,
        "nombre": "SIMPLICITY ROSARIO",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700255,
        "nombre": "SIMPLICITY LISANDRO DE LA TORRE",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700256,
        "nombre": "FARMACITY LA PAMPA",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700257,
        "nombre": "FARMACITY C. LAS ROSAS",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700258,
        "nombre": "FARMACITY VELEZ SARFIELD",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700259,
        "nombre": "FARMACITY NUÑEZ",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700260,
        "nombre": "GET THE LOOK TERRAZAS",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700261,
        "nombre": "FARMACITY S. A.",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700262,
        "nombre": "FARMACITY S. A. CORDOBA",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700429,
        "nombre": "FARMACITY CABA",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 450418,
        "nombre": "FARMACITY SEGUROLA",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 170150,
        "nombre": "FARMACITY",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700221,
        "nombre": "SIMPLICITY ZARATE",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700169,
        "nombre": "FARMACITY HIDALGO",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 700186,
        "nombre": "GET THE LOOK ECUADOR",
        "estado": true,
        "id_cadena": 52
    },
    {
        "id": 700094,
        "nombre": "FARMACITY SUCRE",
        "estado": true,
        "id_cadena": 13
    },
    {
        "id": 702369,
        "nombre": "FARMACITY S.A",
        "estado": true,
        "id_cadena": 13
    }
];