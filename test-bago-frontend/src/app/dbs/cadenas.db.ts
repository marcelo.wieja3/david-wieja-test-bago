export const cadenas = [
    {
        "id": 205,
        "nombre": "25 DE MAYO"
    },
    {
        "id": 511,
        "nombre": "REAL"
    },
    {
        "id": 610,
        "nombre": "AMUSIM"
    },
    {
        "id": 185,
        "nombre": "FARMA PLUS"
    },
    {
        "id": 30,
        "nombre": "CHESTER"
    },
    {
        "id": 469,
        "nombre": "NUEVA PASTEUR"
    },
    {
        "id": 223,
        "nombre": "AMMA"
    },
    {
        "id": 214,
        "nombre": "AGEC"
    },
    {
        "id": 326,
        "nombre": "EMBON"
    },
    {
        "id": 335,
        "nombre": "FARMA LANUS"
    },
    {
        "id": 427,
        "nombre": "MARISCO"
    },
    {
        "id": 594,
        "nombre": "VITAMINA"
    },
    {
        "id": 315,
        "nombre": "DEL PUEBLO NEUQUEN"
    },
    {
        "id": 225,
        "nombre": "ANCLA FARMA"
    },
    {
        "id": 433,
        "nombre": "MATER"
    },
    {
        "id": 190,
        "nombre": "GOMEZ"
    },
    {
        "id": 572,
        "nombre": "SUR"
    },
    {
        "id": 655,
        "nombre": "SANTA ANA BS AS"
    },
    {
        "id": 208,
        "nombre": "A.M.U.R. SANTA FE"
    },
    {
        "id": 491,
        "nombre": "PAUTASSO"
    },
    {
        "id": 352,
        "nombre": "VIDONI"
    },
    {
        "id": 478,
        "nombre": "OZAMIS"
    },
    {
        "id": 528,
        "nombre": "SAN BENITO"
    },
    {
        "id": 240,
        "nombre": "AVILA"
    },
    {
        "id": 134,
        "nombre": "SAN MARTIN"
    },
    {
        "id": 300,
        "nombre": "CURA ALVAREZ"
    },
    {
        "id": 332,
        "nombre": "FANESSI"
    },
    {
        "id": 650,
        "nombre": "A.M.U.R. PARANA"
    },
    {
        "id": 250,
        "nombre": "BELTRAN"
    },
    {
        "id": 667,
        "nombre": "LUJAN II"
    },
    {
        "id": 84,
        "nombre": "MUTUAL"
    },
    {
        "id": 626,
        "nombre": "MODERNA PARANA"
    },
    {
        "id": 513,
        "nombre": "RED COLON"
    },
    {
        "id": 313,
        "nombre": "DEL NORTE"
    },
    {
        "id": 563,
        "nombre": "SOLDINI"
    },
    {
        "id": 220,
        "nombre": "ALVEAR"
    },
    {
        "id": 398,
        "nombre": "LA ESTACION"
    },
    {
        "id": 304,
        "nombre": "DARUICH"
    },
    {
        "id": 605,
        "nombre": "RASPO"
    },
    {
        "id": 288,
        "nombre": "COFARSUR"
    },
    {
        "id": 264,
        "nombre": "BRIATA"
    },
    {
        "id": 603,
        "nombre": "ORLANDI"
    },
    {
        "id": 402,
        "nombre": "LACARRA"
    },
    {
        "id": 431,
        "nombre": "MASTRONARDI"
    },
    {
        "id": 418,
        "nombre": "MALUCCHI"
    },
    {
        "id": 645,
        "nombre": "ZONA VITAL BARILOCHE"
    },
    {
        "id": 612,
        "nombre": "ITALIA"
    },
    {
        "id": 677,
        "nombre": "SEPPI DE VIANO"
    },
    {
        "id": 640,
        "nombre": "LLANEZA"
    },
    {
        "id": 514,
        "nombre": "RED PERSCE"
    },
    {
        "id": 347,
        "nombre": "FAR-MAR"
    },
    {
        "id": 84,
        "nombre": "LA ESQUINA"
    },
    {
        "id": 608,
        "nombre": "MEDIVEN"
    },
    {
        "id": 373,
        "nombre": "GLOBAL"
    },
    {
        "id": 182,
        "nombre": "DANESA"
    },
    {
        "id": 568,
        "nombre": "STIA"
    },
    {
        "id": 85,
        "nombre": "LA SANTE"
    },
    {
        "id": 375,
        "nombre": "GRAL PAZ"
    },
    {
        "id": 42,
        "nombre": "DEL PUENTE"
    },
    {
        "id": 337,
        "nombre": "FARMA SHOP"
    },
    {
        "id": 162,
        "nombre": "LA UNION"
    },
    {
        "id": 473,
        "nombre": "OPENFARMA"
    },
    {
        "id": 261,
        "nombre": "BRADEL"
    },
    {
        "id": 527,
        "nombre": "SALUD GLOBAL"
    },
    {
        "id": 453,
        "nombre": "MOSCONI"
    },
    {
        "id": 167,
        "nombre": "MORI"
    },
    {
        "id": 121,
        "nombre": "PUNTO DE SALUD"
    },
    {
        "id": 244,
        "nombre": "BANCARIA URUG."
    },
    {
        "id": 298,
        "nombre": "CORRALES"
    },
    {
        "id": 363,
        "nombre": "FRATUCCELO"
    },
    {
        "id": 281,
        "nombre": "CENTRO"
    },
    {
        "id": 542,
        "nombre": "SAN PANTALEON"
    },
    {
        "id": 562,
        "nombre": "SOLARI"
    },
    {
        "id": 299,
        "nombre": "CRUZ VERDE"
    },
    {
        "id": 343,
        "nombre": "FARMAFUL"
    },
    {
        "id": 583,
        "nombre": "TUPUNGATO"
    },
    {
        "id": 252,
        "nombre": "BERNINI"
    },
    {
        "id": 39,
        "nombre": "DEL AGUILA MENDOZA"
    },
    {
        "id": 575,
        "nombre": "TANTERA"
    },
    {
        "id": 474,
        "nombre": "ORO VERDE"
    },
    {
        "id": 357,
        "nombre": "FERRO"
    },
    {
        "id": 435,
        "nombre": "MAYO"
    },
    {
        "id": 553,
        "nombre": "SELMA - OTROS"
    },
    {
        "id": 450,
        "nombre": "MOREIRA"
    },
    {
        "id": 665,
        "nombre": "SELLA"
    },
    {
        "id": 276,
        "nombre": "CASTELLI"
    },
    {
        "id": 566,
        "nombre": "SOY"
    },
    {
        "id": 406,
        "nombre": "LIBERTY"
    },
    {
        "id": 547,
        "nombre": "SANTE"
    },
    {
        "id": 324,
        "nombre": "EL PORTAL"
    },
    {
        "id": 625,
        "nombre": "FENIX ROSARIO"
    },
    {
        "id": 647,
        "nombre": "MUTUAL FEDERADA ROSARIO"
    },
    {
        "id": 602,
        "nombre": "ROSMAR"
    },
    {
        "id": 442,
        "nombre": "MINUZZI"
    },
    {
        "id": 502,
        "nombre": "MOLINA"
    },
    {
        "id": 235,
        "nombre": "ASOCIACION MUTUAL FERROVIARIA"
    },
    {
        "id": 280,
        "nombre": "CENTRAL OESTE"
    },
    {
        "id": 643,
        "nombre": "AMERICA"
    },
    {
        "id": 41,
        "nombre": "DEL PLATA"
    },
    {
        "id": 664,
        "nombre": "BRUNO SERGIO"
    },
    {
        "id": 262,
        "nombre": "BRAVO"
    },
    {
        "id": 570,
        "nombre": "SUIZA"
    },
    {
        "id": 447,
        "nombre": "MONICA GRAMAGLIA"
    },
    {
        "id": 237,
        "nombre": "AUSTRAL"
    },
    {
        "id": 632,
        "nombre": "SARTI"
    },
    {
        "id": 506,
        "nombre": "PUEYRREDON"
    },
    {
        "id": 619,
        "nombre": "DEL AGUILA JOSE C PAZ"
    },
    {
        "id": 238,
        "nombre": "AVELLANEDA"
    },
    {
        "id": 405,
        "nombre": "LEVATINO"
    },
    {
        "id": 485,
        "nombre": "PANQUEGUA"
    },
    {
        "id": 275,
        "nombre": "PROTEGER SALUD"
    },
    {
        "id": 691,
        "nombre": "FOCHI"
    },
    {
        "id": 440,
        "nombre": "MESURA"
    },
    {
        "id": 272,
        "nombre": "CARDOSO"
    },
    {
        "id": 118,
        "nombre": "PLAZA"
    },
    {
        "id": 582,
        "nombre": "TUNUYAN"
    },
    {
        "id": 589,
        "nombre": "VESPE"
    },
    {
        "id": 123,
        "nombre": "QUINTANA"
    },
    {
        "id": 624,
        "nombre": "DEL PUEBLO TUCUMAN"
    },
    {
        "id": 477,
        "nombre": "OSORIO"
    },
    {
        "id": 509,
        "nombre": "RAUEK"
    },
    {
        "id": 382,
        "nombre": "GUTIERREZ"
    },
    {
        "id": 630,
        "nombre": "FARMASER"
    },
    {
        "id": 323,
        "nombre": "ECHEGARAY"
    },
    {
        "id": 243,
        "nombre": "BALLOFET"
    },
    {
        "id": 362,
        "nombre": "FRANCIA"
    },
    {
        "id": 215,
        "nombre": "AGUILA"
    },
    {
        "id": 204,
        "nombre": "24 HS"
    },
    {
        "id": 289,
        "nombre": "COGLIATTI"
    },
    {
        "id": 45,
        "nombre": "DEQUINO"
    },
    {
        "id": 347,
        "nombre": "FAR-MAR TUCUMAN"
    },
    {
        "id": 505,
        "nombre": "PRIMAVERA"
    },
    {
        "id": 339,
        "nombre": "FARMA VIP"
    },
    {
        "id": 333,
        "nombre": "FARADAY"
    },
    {
        "id": 587,
        "nombre": "VASCHETTI"
    },
    {
        "id": 627,
        "nombre": "PLAZA CORDOBA"
    },
    {
        "id": 648,
        "nombre": "MUTUAL FEDERADA BAHIA BLANCA"
    },
    {
        "id": 296,
        "nombre": "CORIMAYO"
    },
    {
        "id": 666,
        "nombre": "NUEVA GRAL PAZ"
    },
    {
        "id": 580,
        "nombre": "TRUJUI"
    },
    {
        "id": 559,
        "nombre": "FARMAVITUS"
    },
    {
        "id": 236,
        "nombre": "ASTIGIANO"
    },
    {
        "id": 429,
        "nombre": "MARTINEZ 2"
    },
    {
        "id": 465,
        "nombre": "NUEVA"
    },
    {
        "id": 659,
        "nombre": "ROMY"
    },
    {
        "id": 512,
        "nombre": "RED COFARAL"
    },
    {
        "id": 279,
        "nombre": "CENTRAL"
    },
    {
        "id": 680,
        "nombre": "SELMA - OTROS"
    },
    {
        "id": 670,
        "nombre": "HASENKAMP"
    },
    {
        "id": 596,
        "nombre": "WAL-MART"
    },
    {
        "id": 517,
        "nombre": "REYNOLD"
    },
    {
        "id": 361,
        "nombre": "FRANCE"
    },
    {
        "id": 591,
        "nombre": "VILELA"
    },
    {
        "id": 600,
        "nombre": "HOLGADO"
    },
    {
        "id": 544,
        "nombre": "SANCHEZ ANTONIOLLI"
    },
    {
        "id": 44,
        "nombre": "DEL VALLE"
    },
    {
        "id": 391,
        "nombre": "JERARQUICO SALUD SANTA FE"
    },
    {
        "id": 386,
        "nombre": "INSTITUTO DEL NORTE"
    },
    {
        "id": 338,
        "nombre": "FARMA VIDA"
    },
    {
        "id": 278,
        "nombre": "CENTENARIO"
    },
    {
        "id": 261,
        "nombre": "AMERICA TUCUMAN"
    },
    {
        "id": 247,
        "nombre": "BELGRANO ROSARIO"
    },
    {
        "id": 579,
        "nombre": "TRUCCO"
    },
    {
        "id": 99,
        "nombre": "NUEVA PATRIA"
    },
    {
        "id": 623,
        "nombre": "DEL PUEBLO SANTA FE"
    },
    {
        "id": 319,
        "nombre": "DEL SUD"
    },
    {
        "id": 483,
        "nombre": "PALAZZINI"
    },
    {
        "id": 613,
        "nombre": "NOROESTE"
    },
    {
        "id": 634,
        "nombre": "NUEVA POMPEYA CORRIENTES"
    },
    {
        "id": 482,
        "nombre": "PALAU"
    },
    {
        "id": 253,
        "nombre": "BERTOLO"
    },
    {
        "id": 211,
        "nombre": "ACTIS"
    },
    {
        "id": 345,
        "nombre": "FARMALIFE NEA"
    },
    {
        "id": 417,
        "nombre": "MALPASSI"
    },
    {
        "id": 548,
        "nombre": "SARME"
    },
    {
        "id": 364,
        "nombre": "FUENTES"
    },
    {
        "id": 530,
        "nombre": "SAN CAYETANO"
    },
    {
        "id": 415,
        "nombre": "MADRAS"
    },
    {
        "id": 555,
        "nombre": "SERRANO"
    },
    {
        "id": 297,
        "nombre": "CORRADI"
    },
    {
        "id": 564,
        "nombre": "SOLIDARIA"
    },
    {
        "id": 355,
        "nombre": "FENOGLIO"
    },
    {
        "id": 271,
        "nombre": "CAMI"
    },
    {
        "id": 464,
        "nombre": "NOVARINO"
    },
    {
        "id": 637,
        "nombre": "RP PHARMA"
    },
    {
        "id": 115,
        "nombre": "PINOS DE ANCHORENA"
    },
    {
        "id": 380,
        "nombre": "GT TAURO"
    },
    {
        "id": 396,
        "nombre": "KRAUSEN"
    },
    {
        "id": 614,
        "nombre": "SAN DAMIAN"
    },
    {
        "id": 385,
        "nombre": "I.S.S.U.N.N.E."
    },
    {
        "id": 340,
        "nombre": "FARMA VITAL"
    },
    {
        "id": 535,
        "nombre": "SAN JORGE"
    },
    {
        "id": 314,
        "nombre": "DEL PARANA SCS"
    },
    {
        "id": 617,
        "nombre": "DEL AGUILA RINCON"
    },
    {
        "id": 234,
        "nombre": "ASOCIACION MUTUAL"
    },
    {
        "id": 393,
        "nombre": "JUAREZ RUFINO"
    },
    {
        "id": 654,
        "nombre": "BUTI"
    },
    {
        "id": 202,
        "nombre": "DEL CARMEN"
    },
    {
        "id": 620,
        "nombre": "PREVILEY"
    },
    {
        "id": 663,
        "nombre": "MASINO"
    },
    {
        "id": 488,
        "nombre": "PARQUE ESPAÑA"
    },
    {
        "id": 649,
        "nombre": "FARMALIFE SANTIAGO DEL ESTERO"
    },
    {
        "id": 639,
        "nombre": "CURTO"
    },
    {
        "id": 421,
        "nombre": "MARCOLINI"
    },
    {
        "id": 347,
        "nombre": "FAR-MAR NEA"
    },
    {
        "id": 660,
        "nombre": "SIMILIA"
    },
    {
        "id": 502,
        "nombre": "PLAZOLETA"
    },
    {
        "id": 258,
        "nombre": "BOREAL"
    },
    {
        "id": 226,
        "nombre": "ANDREOLI"
    },
    {
        "id": 518,
        "nombre": "ROCA"
    },
    {
        "id": 646,
        "nombre": "FARMALIFE SANTA FE"
    },
    {
        "id": 266,
        "nombre": "BUENAVENTURA"
    },
    {
        "id": 494,
        "nombre": "PEREYRA"
    },
    {
        "id": 358,
        "nombre": "FILIPINI"
    },
    {
        "id": 521,
        "nombre": "RP WENT"
    },
    {
        "id": 543,
        "nombre": "SANATORIO ALLENDE"
    },
    {
        "id": 206,
        "nombre": "9 DE JULIO"
    },
    {
        "id": 356,
        "nombre": "FERNANDA"
    },
    {
        "id": 227,
        "nombre": "ANIKA"
    },
    {
        "id": 348,
        "nombre": "FARMASAL"
    },
    {
        "id": 499,
        "nombre": "PIEVE"
    },
    {
        "id": 588,
        "nombre": "VERRILLI"
    },
    {
        "id": 458,
        "nombre": "MUTUAL FEDERADA"
    },
    {
        "id": 3,
        "nombre": "ALBERDI PARANA"
    },
    {
        "id": 241,
        "nombre": "AYACUCHO"
    },
    {
        "id": 484,
        "nombre": "PALERMO"
    },
    {
        "id": 267,
        "nombre": "BURGOS"
    },
    {
        "id": 407,
        "nombre": "LLANOS"
    },
    {
        "id": 437,
        "nombre": "MEDICARLO"
    },
    {
        "id": 677,
        "nombre": "NEXOPHARMA"
    },
    {
        "id": 606,
        "nombre": "SUPERMERCADOS LA REINA"
    },
    {
        "id": 611,
        "nombre": "FARMA SRL"
    },
    {
        "id": 391,
        "nombre": "JERARQUICO SALUD"
    },
    {
        "id": 480,
        "nombre": "PACHECO"
    },
    {
        "id": 458,
        "nombre": "MUTUAL FEDERADA CORDOBA"
    },
    {
        "id": 597,
        "nombre": "ZARATE"
    },
    {
        "id": 419,
        "nombre": "MANZONI"
    },
    {
        "id": 368,
        "nombre": "GALIGANI"
    },
    {
        "id": 569,
        "nombre": "STRELUK"
    },
    {
        "id": 50,
        "nombre": "FAMILY FARM"
    },
    {
        "id": 653,
        "nombre": "RECHE-FADELI"
    },
    {
        "id": 662,
        "nombre": "LA FRANCO DEL SUR"
    },
    {
        "id": 311,
        "nombre": "DEL CENTRO"
    },
    {
        "id": 397,
        "nombre": "LA BANCARIA"
    },
    {
        "id": 585,
        "nombre": "UOM LANUS"
    },
    {
        "id": 378,
        "nombre": "GRASSI"
    },
    {
        "id": 212,
        "nombre": "ADMIFAR"
    },
    {
        "id": 661,
        "nombre": "PROBATIS"
    },
    {
        "id": 208,
        "nombre": "A.M.U.R."
    },
    {
        "id": 472,
        "nombre": "OGGI"
    },
    {
        "id": 302,
        "nombre": "DANIELE"
    },
    {
        "id": 425,
        "nombre": "MARINA"
    },
    {
        "id": 209,
        "nombre": "ABC"
    },
    {
        "id": 504,
        "nombre": "PONTE"
    },
    {
        "id": 153,
        "nombre": "VILLEGAS"
    },
    {
        "id": 307,
        "nombre": "DE LA SANTA CRUZ"
    },
    {
        "id": 592,
        "nombre": "VILLAREAL"
    },
    {
        "id": 439,
        "nombre": "MENDOZA"
    },
    {
        "id": 354,
        "nombre": "FENIX"
    },
    {
        "id": 176,
        "nombre": "CARRILLO"
    },
    {
        "id": 377,
        "nombre": "GRANADERO"
    },
    {
        "id": 622,
        "nombre": "BELGRANO MENDOZA"
    },
    {
        "id": 287,
        "nombre": "COFARAL"
    },
    {
        "id": 257,
        "nombre": "BORDA"
    },
    {
        "id": 218,
        "nombre": "ALEM"
    },
    {
        "id": 17,
        "nombre": "BAUZA"
    },
    {
        "id": 565,
        "nombre": "SOLIS"
    },
    {
        "id": 290,
        "nombre": "CON SALUD"
    },
    {
        "id": 638,
        "nombre": "PARNAS"
    },
    {
        "id": 454,
        "nombre": "MUNDIAL"
    },
    {
        "id": 351,
        "nombre": "FAVARIN"
    },
    {
        "id": 255,
        "nombre": "BONACCI"
    },
    {
        "id": 489,
        "nombre": "PASTERIS"
    },
    {
        "id": 524,
        "nombre": "SABIN"
    },
    {
        "id": 601,
        "nombre": "MAGA SHOP EXPRESS"
    },
    {
        "id": 410,
        "nombre": "LOPEZ"
    },
    {
        "id": 318,
        "nombre": "DEL SOL"
    },
    {
        "id": 459,
        "nombre": "NANCY SEGURA"
    },
    {
        "id": 414,
        "nombre": "LUJAN"
    },
    {
        "id": 658,
        "nombre": "MARTINEZ"
    },
    {
        "id": 254,
        "nombre": "BETI"
    },
    {
        "id": 13,
        "nombre": "FARMACITY"
    },
    {
        "id": 401,
        "nombre": "LA PAZ"
    },
    {
        "id": 428,
        "nombre": "MARKETING FARM"
    },
    {
        "id": 631,
        "nombre": "MUTUAL MERCANTIL"
    },
    {
        "id": 87,
        "nombre": "LIDER"
    },
    {
        "id": 273,
        "nombre": "CARUSO"
    },
    {
        "id": 270,
        "nombre": "CAFFARATI"
    },
    {
        "id": 470,
        "nombre": "NUEVA POMPEYA"
    },
    {
        "id": 604,
        "nombre": "MISEGROUP"
    },
    {
        "id": 607,
        "nombre": "MAIPU R"
    },
    {
        "id": 228,
        "nombre": "ANTONELLO"
    },
    {
        "id": 330,
        "nombre": "ESTRELLA"
    },
    {
        "id": 590,
        "nombre": "VIGLIOCO"
    },
    {
        "id": 384,
        "nombre": "HP FARMA"
    },
    {
        "id": 233,
        "nombre": "ASOC. BANCARIA PARANA"
    },
    {
        "id": 320,
        "nombre": "DI FARMA"
    },
    {
        "id": 231,
        "nombre": "ARRIAGA"
    },
    {
        "id": 151,
        "nombre": "VASSALLO"
    },
    {
        "id": 133,
        "nombre": "SAN FRANCISCO"
    },
    {
        "id": 239,
        "nombre": "AVENIDA"
    },
    {
        "id": 246,
        "nombre": "BELEN"
    },
    {
        "id": 422,
        "nombre": "MARCOS"
    },
    {
        "id": 541,
        "nombre": "SAN MATEO"
    },
    {
        "id": 479,
        "nombre": "PACCE"
    },
    {
        "id": 282,
        "nombre": "CERATTO"
    },
    {
        "id": 421,
        "nombre": "CONTINI"
    },
    {
        "id": 70,
        "nombre": "FORNERO"
    },
    {
        "id": 229,
        "nombre": "ARIES"
    },
    {
        "id": 581,
        "nombre": "TULUM"
    },
    {
        "id": 457,
        "nombre": "MUTUAL"
    },
    {
        "id": 451,
        "nombre": "MORENO"
    },
    {
        "id": 27,
        "nombre": "CATEDRAL"
    },
    {
        "id": 641,
        "nombre": "RED GRAL PAZ POSADAS"
    },
    {
        "id": 507,
        "nombre": "PUNTOFARMA"
    },
    {
        "id": 529,
        "nombre": "SAN CAMILO"
    },
    {
        "id": 526,
        "nombre": "SAGRADA FAMILIA"
    },
    {
        "id": 637,
        "nombre": "SEC - RP PHARMA"
    },
    {
        "id": 306,
        "nombre": "DE LA ROSA"
    },
    {
        "id": 189,
        "nombre": "AMBAR"
    },
    {
        "id": 635,
        "nombre": "IPHARM"
    },
    {
        "id": 230,
        "nombre": "ARIEZ"
    },
    {
        "id": 156,
        "nombre": "ZONA VITAL"
    },
    {
        "id": 360,
        "nombre": "FOCHI"
    },
    {
        "id": 369,
        "nombre": "GARCIA COCCO"
    },
    {
        "id": 413,
        "nombre": "LUCIONI"
    },
    {
        "id": 404,
        "nombre": "LAURINO"
    },
    {
        "id": 329,
        "nombre": "ESTILO SALUD"
    },
    {
        "id": 325,
        "nombre": "ELESE"
    },
    {
        "id": 367,
        "nombre": "GAICH"
    },
    {
        "id": 316,
        "nombre": "DEL SARTO"
    },
    {
        "id": 303,
        "nombre": "DANIOTTI"
    },
    {
        "id": 522,
        "nombre": "RS"
    },
    {
        "id": 599,
        "nombre": "SELMA JORGE"
    },
    {
        "id": 434,
        "nombre": "MAXIFARMA"
    },
    {
        "id": 277,
        "nombre": "CAVATORTA"
    },
    {
        "id": 609,
        "nombre": "PILATTI"
    },
    {
        "id": 577,
        "nombre": "TERAN DE SOL A SOL"
    },
    {
        "id": 556,
        "nombre": "SILVA"
    },
    {
        "id": 383,
        "nombre": "HERMIDA"
    },
    {
        "id": 291,
        "nombre": "CONCEPCION"
    },
    {
        "id": 621,
        "nombre": "ALBERDI ROSARIO"
    },
    {
        "id": 657,
        "nombre": "SAN JOSE"
    },
    {
        "id": 388,
        "nombre": "IOSFA"
    },
    {
        "id": 523,
        "nombre": "RUTINI"
    },
    {
        "id": 207,
        "nombre": "A.M.E.B."
    },
    {
        "id": 334,
        "nombre": "FARMA 24 (JAIMOVICH)"
    },
    {
        "id": 537,
        "nombre": "SAN LUIS"
    },
    {
        "id": 359,
        "nombre": "FLORIDA"
    },
    {
        "id": 108,
        "nombre": "PARADIÑEIRO"
    },
    {
        "id": 573,
        "nombre": "SUTIAGA"
    },
    {
        "id": 536,
        "nombre": "SAN JUAN"
    },
    {
        "id": 551,
        "nombre": "SCIENZA"
    },
    {
        "id": 487,
        "nombre": "PARQUE"
    },
    {
        "id": 126,
        "nombre": "RIADIGOS"
    },
    {
        "id": 412,
        "nombre": "LOS PERALES"
    },
    {
        "id": 317,
        "nombre": "DEL SIGLO"
    },
    {
        "id": 561,
        "nombre": "SOCIAL LUZ Y FUERZA"
    },
    {
        "id": 671,
        "nombre": "BARILARI"
    },
    {
        "id": 136,
        "nombre": "SANTA ANA"
    },
    {
        "id": 498,
        "nombre": "PIERISTEI"
    },
    {
        "id": 468,
        "nombre": "NUEVA ITALIA"
    },
    {
        "id": 460,
        "nombre": "NATAL"
    },
    {
        "id": 249,
        "nombre": "BELLO"
    },
    {
        "id": 411,
        "nombre": "LOS ALAMOS"
    },
    {
        "id": 475,
        "nombre": "ORTIZ"
    },
    {
        "id": 525,
        "nombre": "SAGITARIO"
    },
    {
        "id": 242,
        "nombre": "BADIA"
    },
    {
        "id": 345,
        "nombre": "FARMALIFE"
    },
    {
        "id": 292,
        "nombre": "CONCORDIA"
    },
    {
        "id": 481,
        "nombre": "PAIDON"
    },
    {
        "id": 642,
        "nombre": "FARMASERV"
    },
    {
        "id": 265,
        "nombre": "BRISTOL"
    },
    {
        "id": 519,
        "nombre": "ROJAS"
    },
    {
        "id": 260,
        "nombre": "BOTTO"
    },
    {
        "id": 516,
        "nombre": "RED PHARMA"
    },
    {
        "id": 344,
        "nombre": "FARMAGRE"
    },
    {
        "id": 466,
        "nombre": "NUEVA ALBARDON"
    },
    {
        "id": 245,
        "nombre": "BANDA"
    },
    {
        "id": 669,
        "nombre": "POLINI"
    },
    {
        "id": 455,
        "nombre": "MUSURUANA"
    },
    {
        "id": 576,
        "nombre": "TEKIEL"
    },
    {
        "id": 286,
        "nombre": "COCCARO"
    },
    {
        "id": 341,
        "nombre": "FARMACIE"
    },
    {
        "id": 629,
        "nombre": "AVILA ROSARIO"
    },
    {
        "id": 656,
        "nombre": "URQUIZA"
    },
    {
        "id": 636,
        "nombre": "MOSCOVICH"
    },
    {
        "id": 43,
        "nombre": "DEL PUERTO"
    },
    {
        "id": 560,
        "nombre": "SOCIAL FENIX"
    },
    {
        "id": 644,
        "nombre": "ROMAVIKAR"
    },
    {
        "id": 374,
        "nombre": "GODOY CRUZ"
    },
    {
        "id": 391,
        "nombre": "JERARQUICO SALUD ENTRE RIOS"
    },
    {
        "id": 420,
        "nombre": "MARCHETTI BEATRIZ"
    },
    {
        "id": 633,
        "nombre": "LA CAPITAL ROSARIO"
    },
    {
        "id": 668,
        "nombre": "RED PASCO"
    }
];