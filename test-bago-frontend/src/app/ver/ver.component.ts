import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Farmacia } from '../models/farmacia.model';

@Component({
  selector: 'app-ver',
  templateUrl: './ver.component.html',
  styleUrls: ['./ver.component.scss']
})
export class VerComponent implements OnInit {

  displayedColumns: string[] = ['id', 'nombre', 'estado', "id_cadena"];

  farmacias: Farmacia[] = [];
  farmaciasFiltradas: Farmacia[] = [];

  static ID_CADENA_FARMACITY: number = 13;

  api = {};
  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.apiService.getFarmacias().subscribe((farmacias: any)=>{  
      this.farmacias = this.farmaciasFiltradas; 
      this.farmaciasFiltradas = farmacias.filter(f => f.id_cadena === VerComponent.ID_CADENA_FARMACITY);
    });
  }

}
