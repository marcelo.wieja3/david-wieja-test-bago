import { Component, OnInit } from '@angular/core';
import { MatPaginator } from '@angular/material';
import { Observable } from 'rxjs';
import { ApiService } from '../api.service';
import { Cadena } from '../models/cadena.model';
import { Farmacia } from '../models/farmacia.model';

@Component({
  selector: 'app-one',
  templateUrl: './one.component.html',
  styleUrls: ['./one.component.scss']
})
export class OneComponent implements OnInit {

  api = {};
  constructor(private apiService: ApiService) { }

  ngOnInit() {

    this.apiService.getDemoMethod().subscribe((data: any)=>{  
      this.api = data;  
    });


  }

}
