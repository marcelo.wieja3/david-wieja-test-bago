import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Cadena } from '../models/cadena.model';
import { Farmacia } from '../models/farmacia.model';

@Component({
  selector: 'app-alta',
  templateUrl: './alta.component.html',
  styleUrls: ['./alta.component.scss']
})
export class AltaComponent implements OnInit {

  cadenas: Cadena[] = [];

  farmacia: Farmacia = this.nuevaFarmacia();

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.apiService.getCadenas().subscribe(cadenas => this.cadenas = cadenas);
  }

  activarFarmacia() {
    this.farmacia.estado = !this.farmacia.estado;
  }

  guardarFarmacia() {
    this.apiService.saveFarmacia(this.farmacia);
    this.farmacia = this.nuevaFarmacia();
    alert('La nueva farmacia se guardo correctamente!')
  }

  seleccionarCadena($event) {
    console.log($event)
    this.farmacia.id_cadena = $event.value;
  }

  private nuevaFarmacia(): Farmacia {
    return {id: undefined, nombre: '', id_cadena: 0, estado: false };
  }

}
