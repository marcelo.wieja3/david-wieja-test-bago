import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';
import { Farmacia } from './models/farmacia.model';
import { farmacias } from './dbs/farmacias.db';
import { Cadena } from './models/cadena.model';
import { cadenas } from './dbs/cadenas.db';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  farmacias$: BehaviorSubject<Farmacia[]> = new BehaviorSubject<Farmacia[]>(farmacias);

  cadenas$: BehaviorSubject<Cadena[]> = new BehaviorSubject<Cadena[]>(cadenas);

  constructor(private httpClient: HttpClient) { 

  }

  public getDemoMethod(){  
		return this.httpClient.get(`${environment.apiUrl}/demo`);  
	}  

  public getFarmacias(): Observable<Farmacia[]> {  
		// return this.httpClient.get(`${environment.apiUrl}/farmacias`);  
    return this.farmacias$;
	}  

  public saveFarmacia(farmacia: Farmacia): Farmacia {  
		// return this.httpClient.post(`${environment.apiUrl}/farmacias`, farmacia);  
    farmacia.id = Date.now();
    const farmaciasActual = this.farmacias$.value;
    farmaciasActual.push(farmacia)
    this.farmacias$.next(farmaciasActual);
    return farmacia; 
	}  

  public getCadenas(): Observable<Cadena[]> {  
		// return this.httpClient.get(`${environment.apiUrl}/cadenas`);  
    return this.cadenas$;
	}  


}